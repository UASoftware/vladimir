import { FlexLayoutModule } from '@angular/flex-layout';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { of } from 'rxjs';
import { CustomError } from 'ts-custom-error';

import { SharedPresentationModule } from '@rhinov/shared/presentation';
import { Command, Query } from '@rhinov/shared/cqrx';
import { GO_TO_FAVORITE_COMMAND_TOKEN } from '@rhinov/shared/application';
import { USER_QUERY_TOKEN, User } from '@rhinov/users/application';
import {
  IS_PROJECT_OWNER_QUERY_TOKEN,
  GO_TO_BURGER_MENU_COMMAND_TOKEN,
  GO_TO_SHARING_COMMAND_TOKEN,
  SHOW_PHONE_COMMAND_TOKEN,
  PAGE_TYPE_QUERY_TOKEN,
} from '@rhinov/delivering/application';
import { UserWidgetConfig, USER_WIDGET_CONFIG_TOKEN } from '@rhinov/shared/user-widget';

import { TopBarComponent } from './top-bar.component';
import { action } from '@storybook/addon-actions';

const customViewports = {
  topBarScreen: {
    name: 'Top bar screen',
    styles: {
      width: '1444px',
      height: '939px',
    },
  },
};

export default {
  title: 'Feature| Top bar',
  parameters: {
    // the viewports object from the Essentials addon
    viewport: {
      // the viewports you want to use
      viewports: {
        ...INITIAL_VIEWPORTS,
        ...customViewports,
      },
      // your own default viewport
      // defaultViewport: 'topBarScreen'
    },
  },
};

const userWidgetConfigMock: UserWidgetConfig = {
  apiUrl: 'https://api.rhinov.fr',
  authUrl: 'https://connect.rhinov.fr',
  authLogOutUrl: 'https://connect.rhinov.fr/logout',
  accountUrl: 'https://account.rhinov.fr',
  accountProfileUrl: 'https://account.rhinov.fr/profile',
  accountOrdersUrl: 'https://account.rhinov.fr/orders',
  accountSponsorshipUrl: 'https://account.rhinov.fr/sponsorship',
  orderUrl: 'https://order.rhinov.fr',
  shopOrdersUrl: 'https://shop.rhinov.fr/mon-compte',
  rhinovContact: 'https://www.rhinov.fr/contact/',
  cookiesDomain: '.rhinov.fr',
  userAvatarServer: 'https://api.rhinov.fr/uploads/users/',
};

const queryDef = {
  execute: () => {},
  values$: of(true),
} as Query<boolean>;

const commandDef: Command<undefined, boolean> = {
  execute: () => {},
  pending$: of(false),
  complete$: of(true),
  error$: of(new CustomError()),
};

const onPhoneClickCommandDef: Command<undefined, boolean> = {
  execute: () => {},
  pending$: of(false),
  complete$: of(true),
  error$: of(new CustomError()),
};

const queryUserMock = {
  execute: () => {},
  values$: of({
    id: '13',
    firstName: 'Byelik',
    lastName: 'Bilichenko',
    avatarUrl: '',
  } as User),
} as Query<User>;

const queryPageType = {
  execute: () => {},
  values$: of('home'), // 'home' | 'menu' | 'sub-menu'
};

export const Desktop = () => ({
  moduleMetadata: {
    imports: [SharedPresentationModule, FlexLayoutModule],
    providers: [
      { provide: USER_WIDGET_CONFIG_TOKEN, useValue: userWidgetConfigMock },
      { provide: IS_PROJECT_OWNER_QUERY_TOKEN, useValue: queryDef },
      { provide: GO_TO_BURGER_MENU_COMMAND_TOKEN, useValue: commandDef },
      { provide: GO_TO_SHARING_COMMAND_TOKEN, useValue: commandDef },
      { provide: GO_TO_FAVORITE_COMMAND_TOKEN, useValue: commandDef },
      { provide: GO_TO_FAVORITE_COMMAND_TOKEN, useValue: commandDef },
      { provide: SHOW_PHONE_COMMAND_TOKEN, useValue: onPhoneClickCommandDef },
      { provide: USER_QUERY_TOKEN, useValue: queryUserMock },
      { provide: PAGE_TYPE_QUERY_TOKEN, useValue: queryPageType },
    ],
  },
  component: TopBarComponent,
  props: {
    onBurgerButtonClick: action('onBurgerButtonClick'),

    onFavoriteButtonClick: action('onFavoriteButtonClick'),

    onShareButtonClick: action('onShareButtonClick'),

    onPhoneButtonClick: action('onPhoneButtonClick'),
  },
});
