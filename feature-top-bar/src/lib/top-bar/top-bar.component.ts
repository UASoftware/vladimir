import { Component } from '@angular/core';

import { BaseTopBarComponent } from '../base-top-bar.component';
import { topBarVisibilityAnimation } from '../top-bar-visibility.animation';

@Component({
  selector: 'rhinov-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
  animations: [topBarVisibilityAnimation],
})
export class TopBarComponent extends BaseTopBarComponent {}
