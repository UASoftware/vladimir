import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AnalyticsModule } from '@rhinov/shared/analytics';
import { ScrollableContainerModule } from '@rhinov/shared/cdk';

import { TopBarComponent } from './top-bar/top-bar.component';

@NgModule({
  imports: [CommonModule, FlexLayoutModule, AnalyticsModule, ScrollableContainerModule],
  declarations: [TopBarComponent],
  exports: [TopBarComponent],
})
export class FeatureTopBarModule {}
