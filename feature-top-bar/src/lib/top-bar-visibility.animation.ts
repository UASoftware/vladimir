import { animate, state, style, transition, trigger } from '@angular/animations';

export const topBarVisibilityAnimation = trigger('toggleVisibility', [
  state('hidden', style({ opacity: 0, height: '0' })),
  state('visible', style({ opacity: 1, height: '*' })),
  transition('* => *', animate('200ms ease-in')),
]);
