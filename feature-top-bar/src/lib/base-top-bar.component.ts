import { Inject, OnDestroy, OnInit, SkipSelf, Directive, HostBinding } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { combineLatest, merge, Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, map, mapTo, startWith } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { LiteralUnion } from 'type-fest';

import { Command, Query } from '@rhinov/shared/cqrx';
import { GO_TO_FAVORITE_COMMAND_TOKEN } from '@rhinov/shared/application';
import {
  GO_TO_BURGER_MENU_COMMAND_TOKEN,
  GO_TO_SHARING_COMMAND_TOKEN,
  PAGE_TYPE_QUERY_TOKEN,
  SHOW_PHONE_COMMAND_TOKEN,
  PageType,
  GO_TO_USER_MENU_COMMAND_TOKEN,
  GO_BACK_COMMAND_TOKEN,
} from '@rhinov/delivering/application';
import {
  setupUserWidget,
  setUserData,
  USER_WIDGET_CONFIG_TOKEN,
  UserWidgetConfig,
} from '@rhinov/shared/user-widget';
import {
  USER_QUERY_TOKEN,
  User,
  UserAccess,
  USER_ACCESS_QUERY_TOKEN,
} from '@rhinov/users/application';
import { ScrollableContainerService } from '@rhinov/shared/cdk';

export type VisibilityState = LiteralUnion<'visible' | 'hidden', string>;

@UntilDestroy()
@Directive()
export abstract class BaseTopBarComponent implements OnInit, OnDestroy {
  userAccess$ = this.userAccessQuery.values$;
  pageType$: Observable<PageType>;

  user?: User;

  private visibilityState: VisibilityState = 'visible';

  private subscription: Subscription | null = null;

  private readonly routerNavigationEnd$ = this.router.events.pipe(
    startWith(''),
    filter((event) => event instanceof NavigationEnd || typeof event === 'string'),
    map((event) => (typeof event === 'string' ? event : (event as NavigationEnd).url)),
    distinctUntilChanged()
  );

  private readonly visibilityState$: Observable<VisibilityState> = merge(
    combineLatest([
      this.scrollableContainerService.scrollUp$.pipe(startWith('up')),
      this.routerNavigationEnd$,
    ]).pipe(mapTo('visible')),
    this.scrollableContainerService.scrollDown$.pipe(mapTo('hidden'))
  );

  @HostBinding('@toggleVisibility')
  get toggleVisibility(): VisibilityState {
    return this.visibilityState;
  }

  constructor(
    @Inject(GO_BACK_COMMAND_TOKEN)
    protected goBackCommand: Command<undefined, boolean>,
    @Inject(USER_WIDGET_CONFIG_TOKEN)
    protected userWidgetConfig: UserWidgetConfig,
    @Inject(USER_ACCESS_QUERY_TOKEN)
    private userAccessQuery: Query<UserAccess | undefined>,
    @Inject(GO_TO_BURGER_MENU_COMMAND_TOKEN)
    protected onBurgerButtonCommand: Command<undefined, boolean>,
    @SkipSelf()
    @Inject(GO_TO_FAVORITE_COMMAND_TOKEN)
    protected onFavoriteButtonCommand: Command,
    @Inject(GO_TO_SHARING_COMMAND_TOKEN)
    protected onShareButtonCommand: Command<undefined, boolean>,
    @Inject(USER_QUERY_TOKEN)
    protected userQuery: Query<User | undefined>,
    @Inject(SHOW_PHONE_COMMAND_TOKEN)
    protected showPhone: Command<undefined, boolean>,
    @Inject(PAGE_TYPE_QUERY_TOKEN)
    pageTypeQuery: Query<PageType>,
    @Inject(GO_TO_USER_MENU_COMMAND_TOKEN)
    protected showUserMenuCommand: Command<undefined, boolean>,
    private scrollableContainerService: ScrollableContainerService,
    private router: Router
  ) {
    this.pageType$ = pageTypeQuery.values$;

    this.visibilityState$
      .pipe(untilDestroyed(this))
      .subscribe((state) => (this.visibilityState = state));
  }

  ngOnInit(): void {
    this.setUserWidgetConfig(this.userWidgetConfig);

    this.subscription = this.userQuery.values$.subscribe((user) => {
      setUserData(user);
      this.user = user;
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onBurgerButtonClick() {
    this.onBurgerButtonCommand.execute();
  }

  onFavoriteButtonClick() {
    this.onFavoriteButtonCommand.execute();
  }

  onShareButtonClick() {
    this.onShareButtonCommand.execute();
  }

  onPhoneButtonClick() {
    this.showPhone.execute();
  }

  onBack() {
    this.goBackCommand.execute();
  }

  private setUserWidgetConfig(userWidgetConfig: UserWidgetConfig) {
    setupUserWidget({
      API_URL: userWidgetConfig.apiUrl,
      AUTH_URL: userWidgetConfig.authUrl,
      AUTH_LOGOUT_URL: userWidgetConfig.authLogOutUrl,
      ACCOUNT_URL: userWidgetConfig.accountUrl,
      ACCOUNT_PROFILE_URL: userWidgetConfig.accountProfileUrl,
      ACCOUNT_ORDERS_LIST: userWidgetConfig.accountOrdersUrl,
      ACCOUNT_SPONSORSHIP_URL: userWidgetConfig.accountSponsorshipUrl,
      ORDER_URL: userWidgetConfig.orderUrl,
      SHOP_ORDERS_URL: userWidgetConfig.shopOrdersUrl,
      RHINOV_CONTACT: userWidgetConfig.rhinovContact,
      COOKIES_DOMAIN: userWidgetConfig.cookiesDomain,
      USER_AVATAR_SERVER: userWidgetConfig.userAvatarServer,
    });
  }
}
