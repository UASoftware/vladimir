import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { AdvertiseCard } from '@rhinov/promoting/application';
import { AdvertisingCardData, TipsCardData } from '@rhinov/shared/application';
import { ShopItem } from '@rhinov/shopping/application';
import { Article } from '@rhinov/articles/application';

import { ShoppingCard } from '../models';

@Directive()
export abstract class ShoppingGridComponent {
  @Input() shuffledCards: ShoppingCard[] = [];

  @Output() advertiseClick: EventEmitter<AdvertiseCard> = new EventEmitter<AdvertiseCard>();
  @Output() shopItemClick: EventEmitter<ShopItem> = new EventEmitter<ShopItem>();
  @Output() designTipsClick: EventEmitter<Article> = new EventEmitter<Article>();

  slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 1000,
    dots: true,
  };

  private adsCardMouseDownX = 0;
  private adsCardMouseDownY = 0;
  private isAdsCardSwiping = false;

  private readonly distanceFromWhichClicksRejects = 10;

  convertAdCard(card?: AdvertiseCard): AdvertisingCardData {
    return {
      id: '',
      imageSmall: card!.backgroundImage,
      imageLarge: card!.backgroundImageLarge,
      urlLink: card!.linkText,
      imageSmallAdaptive: card!.backgroundImageAdaptive,
      imageLargeAdaptive: card!.backgroundImageLargeAdaptive,
    } as AdvertisingCardData;
  }

  convertTips(tips?: Article): TipsCardData {
    return <TipsCardData>tips;
  }

  onAdvertisingCardClick(advertisingItem: AdvertisingCardData, forceClick = false) {
    if (!forceClick && this.isAdsCardSwiping) {
      return false;
    }
    const adCard = {
      linkText: advertisingItem.urlLink,
    } as AdvertiseCard;
    this.advertiseClick.emit(adCard);
  }

  onAdvertisingCardMouseDown(event: MouseEvent) {
    this.adsCardMouseDownX = event.clientX;
    this.adsCardMouseDownY = event.clientY;
  }

  onAdvertisingCardMouseUp(event: MouseEvent) {
    this.isAdsCardSwiping =
      Math.hypot(event.clientX - this.adsCardMouseDownX, event.clientY - this.adsCardMouseDownY) >
      this.distanceFromWhichClicksRejects;
  }

  onShoppingListDetailsObject(shopItem?: ShopItem) {
    this.shopItemClick.emit(shopItem);
  }

  onDesignTipsClick(article?: Article) {
    this.designTipsClick.emit(article);
  }
}
