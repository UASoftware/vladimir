import { Article } from '@rhinov/articles/application';
import { AdvertiseCard } from '@rhinov/promoting/application';
import { ShopItem } from '@rhinov/shopping/application';

export interface ShoppingCard {
  readonly type: 'shop' | 'ads' | 'ads-array' | 'article';
  readonly shopItem?: ShopItem;
  readonly adsCard?: AdvertiseCard;
  readonly adsCards?: AdvertiseCard[];
  readonly article?: Article;
}
