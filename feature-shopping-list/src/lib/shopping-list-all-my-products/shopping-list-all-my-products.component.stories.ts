import { IconSpriteModule } from 'ng-svg-icon-sprite';
import { FlexLayoutModule } from '@angular/flex-layout';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { action } from '@storybook/addon-actions';
import { of } from 'rxjs';

import { CustomError } from 'ts-custom-error';

import {
  AvailableOffers,
  ShopItem,
  SingleTipsCardsQueryMock,
  TipsCardsCommandMock,
} from '@rhinov/shared/application';
import { SharedPresentationModule } from '@rhinov/shared/presentation';

import { Command } from '@rhinov/shared/cqrx';
import { ShoppingListAllMyProductsComponent } from './shopping-list-all-my-products.component';
import {
  ARTICLES_QUERY_TOKEN,
  READ_MORE_ARTICLE_COMMAND_TOKEN,
} from '@rhinov/articles/application';

import {
  GO_TO_ADVERTISING_WINDOW_COMMAND_TOKEN,
  PROMOTIONAL_IMAGES_QUERY_TOKEN,
} from '@rhinov/promoting/application';
import {
  GO_TO_OBJECT_DETAILS_COMMAND_TOKEN,
  SHOPPING_LIST_QUERY_TOKEN,
} from '@rhinov/shopping/application';

export default {
  title: 'Feature|6 - Ma shopping-list',
  parameters: {
    // the viewports object from the Essentials addon
    viewport: {
      // the viewports you want to use
      viewports: {
        ...INITIAL_VIEWPORTS,
      },
    },
  },
};

const shopItemClickCommand: Command<boolean, boolean> = {
  execute: () => {},
  pending$: of(false),
  complete$: of(true),
  error$: of(new CustomError()),
};

const queryShopItems = {
  execute: () => {},
  values$: of([
    {
      // size: CardSize.NORMAL,
      thumbnailPath:
        'https://shop.rhinov.fr/112611/long-island-table-basse-indus-en-sapin-massif-et-metal.jpg',
      price: '100E',
      name: 'Chair 1',
      logoPath: 'https://shop.rhinov.fr/img/m/7.jpg',
    } as ShopItem,
    {
      // size: CardSize.LARGE,
      thumbnailPath: 'https://shop.rhinov.fr/53839/tradition-chaise-bistrot-blanche.jpg',
      avatarPath: '',
      price: '200E',
      name: 'Chair 2',
      logoPath: 'https://shop.rhinov.fr/img/m/7.jpg',
    } as ShopItem,
    {
      // size: CardSize.NORMAL,
      thumbnailPath:
        'https://shop.rhinov.fr/68700/lot-de-2-chaises-melody-en-kubu-et-metal-beige.jpg',
      price: '200E',
      name: 'Sofa',
      logoPath: 'https://shop.rhinov.fr/img/m/7.jpg',
    } as ShopItem,
    {
      // size: CardSize.NORMAL,
      thumbnailPath:
        'https://shop.rhinov.fr/113163/solstice-bibliotheque-3-portes-cannage-en-rotin.jpg',
      price: '500E',
      name: 'Table',
      logoPath: 'https://shop.rhinov.fr/img/m/7.jpg',
    } as ShopItem,
    {
      // size: CardSize.NORMAL,
      thumbnailPath:
        'https://shop.rhinov.fr/113163/solstice-bibliotheque-3-portes-cannage-en-rotin.jpg',
      price: '50E',
      name: 'chair',
      logoPath: 'https://shop.rhinov.fr/img/m/7.jpg',
    } as ShopItem,
    {
      // size: CardSize.NORMAL,
      thumbnailPath: 'https://shop.rhinov.fr/53839/tradition-chaise-bistrot-blanche.jpg',
      price: '500E',
      name: 'Table',
      logoPath: 'https://shop.rhinov.fr/img/m/7.jpg',
    } as ShopItem,
    {
      // size: CardSize.NORMAL,
      thumbnailPath: 'https://shop.rhinov.fr/53839/tradition-chaise-bistrot-blanche.jpg',
      price: '500E',
      name: 'Table',
      logoPath: 'https://shop.rhinov.fr/img/m/7.jpg',
    } as ShopItem,
  ]),
};

const advertiseClickCommand: Command<boolean, boolean> = {
  execute: () => {},
  pending$: of(false),
  complete$: of(true),
  error$: of(new CustomError()),
};

const queryAdvertiseItems = {
  execute: () => {},
  values$: of([
    {
      // size: CardSize.SMALL,
      bgColor: '#FFF',
      logo: 'https://shop.rhinov.fr/img/m/7.jpg',
      title: 'Frais de port gratuit',
      description:
        'Jusqu’au 10/08/2020, profitez de la livraison gratuite sur tous les produits Outdoor',
      thumbnail: 'https://shop.rhinov.fr/53839/tradition-chaise-bistrot-blanche.jpg',
      barBgColor: '#DEC6A4',
    } as AvailableOffers,
    {
      // size: CardSize.SMALL,
      bgColor: '#1dff21',
      logo: 'https://shop.rhinov.fr/img/m/7.jpg',
      title: 'Frais de port gratuit',
      description:
        'Jusqu’au 10/08/2020, profitez de la livraison gratuite sur tous les produits Outdoor',
      thumbnail: 'https://shop.rhinov.fr/53839/tradition-chaise-bistrot-blanche.jpg',
      barBgColor: '#DEC6A4',
    } as AvailableOffers,
    {
      // size: CardSize.NORMAL,
      bgColor: '#FFF',
      logo: 'https://shop.rhinov.fr/img/m/7.jpg',
      title: 'Frais de port gratuit',
      description:
        'Jusqu’au 10/08/2020, profitez de la livraison gratuite sur tous les produits Outdoor',
      thumbnail: 'https://shop.rhinov.fr/53839/tradition-chaise-bistrot-blanche.jpg',
    } as AvailableOffers,
    {
      // size: CardSize.NORMAL,
      bgColor: '#dec6a4',
      logo: 'https://shop.rhinov.fr/img/m/7.jpg',
      title: 'Frais de port gratuit',
      description:
        'Jusqu’au 10/08/2020, profitez de la livraison gratuite sur tous les produits Outdoor',
      thumbnail: 'https://shop.rhinov.fr/53839/tradition-chaise-bistrot-blanche.jpg',
    } as AvailableOffers,
    {
      // size: CardSize.NORMAL,
      bgColor: '#dec6a4',
      logo: 'https://shop.rhinov.fr/img/m/7.jpg',
      title: 'Frais de port gratuit',
      description:
        'Jusqu’au 10/08/2020, profitez de la livraison gratuite sur tous les produits Outdoor',
      thumbnail: 'https://shop.rhinov.fr/53839/tradition-chaise-bistrot-blanche.jpg',
    } as AvailableOffers,
    {
      // size: CardSize.NORMAL,
      bgColor: '#dec6a4',
      logo: 'https://shop.rhinov.fr/img/m/7.jpg',
      title: 'Frais de port gratuit',
      description:
        'Jusqu’au 10/08/2020, profitez de la livraison gratuite sur tous les produits Outdoor',
      thumbnail: 'https://shop.rhinov.fr/53839/tradition-chaise-bistrot-blanche.jpg',
    } as AvailableOffers,
    {
      // size: CardSize.NORMAL,
      bgColor: '#dec6a4',
      logo: 'https://shop.rhinov.fr/img/m/7.jpg',
      title: 'Frais de port gratuit',
      description:
        'Jusqu’au 10/08/2020, profitez de la livraison gratuite sur tous les produits Outdoor',
      thumbnail: 'https://shop.rhinov.fr/53839/tradition-chaise-bistrot-blanche.jpg',
    } as AvailableOffers,
  ]),
};

export const AllMyProducts = () => ({
  moduleMetadata: {
    imports: [FlexLayoutModule, IconSpriteModule, SharedPresentationModule],
    providers: [
      {
        provide: SHOPPING_LIST_QUERY_TOKEN,
        useValue: queryShopItems,
      },
      {
        provide: GO_TO_OBJECT_DETAILS_COMMAND_TOKEN,
        useValue: shopItemClickCommand,
      },
      {
        provide: PROMOTIONAL_IMAGES_QUERY_TOKEN,
        useValue: queryAdvertiseItems,
      },
      {
        provide: GO_TO_ADVERTISING_WINDOW_COMMAND_TOKEN,
        useValue: advertiseClickCommand,
      },
      {
        provide: ARTICLES_QUERY_TOKEN,
        useValue: SingleTipsCardsQueryMock,
      },
      {
        provide: READ_MORE_ARTICLE_COMMAND_TOKEN,
        useValue: TipsCardsCommandMock,
      },
    ],
  },
  component: ShoppingListAllMyProductsComponent,
  props: {
    shoppingListAction: action('shoppingListAction'),
    onShoppingListDetailsObject: action('onShoppingListDetailsObject'),
    onAdvertisingCardClick: action('onAdvertisingCardClick'),
    onDesignTipsClick: action('onDesignTipsClick'),
  },
});
