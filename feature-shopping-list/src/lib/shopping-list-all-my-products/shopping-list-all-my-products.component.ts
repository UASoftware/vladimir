import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { GoogleTagManagerService } from '@rhinov/shared/analytics';

import { ShuffledCardsPresenter } from '../presenters';
import { BaseShoppingList } from '../base-shopping-list';
import { GO_TO_OBJECT_DETAILS_COMMAND_TOKEN } from '@rhinov/shopping/application';
import { Command } from '@rhinov/shared/cqrx';
import {
  AdvertiseCard,
  GO_TO_ADVERTISING_WINDOW_COMMAND_TOKEN,
} from '@rhinov/promoting/application';
import { Article, READ_MORE_ARTICLE_COMMAND_TOKEN } from '@rhinov/articles/application';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'rhinov-shopping-list-all-my-products',
  templateUrl: './shopping-list-all-my-products.component.html',
  styleUrls: ['./shopping-list-all-my-products.component.scss'],
  providers: [ShuffledCardsPresenter],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ShoppingListAllMyProductsComponent extends BaseShoppingList {
  constructor(
    breakpointObserver: BreakpointObserver,
    googleTagManagerService: GoogleTagManagerService,

    @Inject(GO_TO_OBJECT_DETAILS_COMMAND_TOKEN)
    goToProductDetails: Command<string, boolean>,

    @Inject(GO_TO_ADVERTISING_WINDOW_COMMAND_TOKEN)
    goToAdvertisingItem: Command<AdvertiseCard, boolean>,

    @Inject(READ_MORE_ARTICLE_COMMAND_TOKEN)
    goToDesignTipCard: Command<Article>,

    shuffledCardsPresenter: ShuffledCardsPresenter
  ) {
    super(
      breakpointObserver,
      googleTagManagerService,
      goToProductDetails,
      goToAdvertisingItem,
      goToDesignTipCard,
      shuffledCardsPresenter,
      '',
      'shopping-all'
    );
  }
}
