import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from '@angular/cdk/layout';
import { IconSpriteModule } from 'ng-svg-icon-sprite';

import { SlickCarouselModule } from 'ngx-slick-carousel';
import { RouterModule } from '@angular/router';
import { SharedPresentationModule } from '@rhinov/shared/presentation';
import { ShoppingListAllMyProductsComponent } from './shopping-list-all-my-products/shopping-list-all-my-products.component';
import { ShoppingGridDesktopModule } from './shopping-grid-desktop.module';
import { AnalyticsModule } from '@rhinov/shared/analytics';
import { ScrollableContainerModule } from '@rhinov/shared/cdk';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    LayoutModule,
    IconSpriteModule,
    SharedPresentationModule,
    SlickCarouselModule,
    AnalyticsModule,
    RouterModule.forChild([{ path: '', component: ShoppingListAllMyProductsComponent }]),
    ShoppingGridDesktopModule,
    ScrollableContainerModule,
  ],
  declarations: [ShoppingListAllMyProductsComponent],
})
export class FeatureShoppingListAllMyProductsDesktopModule {}
