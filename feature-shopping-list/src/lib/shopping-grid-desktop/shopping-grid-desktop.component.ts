import { ChangeDetectionStrategy, Component } from '@angular/core';

import { ShoppingGridComponent } from '../shopping-grid/shopping-grid.component';

@Component({
  selector: 'rhinov-shopping-grid-desktop',
  templateUrl: './shopping-grid-desktop.component.html',
  styleUrls: ['./shopping-grid-desktop.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ShoppingGridDesktopComponent extends ShoppingGridComponent {}
