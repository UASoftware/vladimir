import { Directive, OnInit } from '@angular/core';
import { ShuffledCardsPresenter } from './presenters';
import { Observable } from 'rxjs';
import { ShopItem } from '@rhinov/shopping/application';
import { AdvertiseCard, PromotionalImagesLanding } from '@rhinov/promoting/application';
import { Article } from '@rhinov/articles/application';
import { ShoppingCard } from './models';
import { Command } from '@rhinov/shared/cqrx';
import { AdvertisingCardData } from '@rhinov/shared/application';
import { GoogleTagManagerService } from '@rhinov/shared/analytics';
import { distinctUntilChanged, filter, tap } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@UntilDestroy()
@Directive()
export abstract class BaseShoppingList implements OnInit {
  shuffledCards$: Observable<ShoppingCard[]>;

  private readonly shopItemsForPage: string | undefined;
  private readonly advertisingForPage: PromotionalImagesLanding;

  private readonly mediaBreakpointsToNumberColumns = {
    '(min-width: 1440px)': 4,
    '(min-width: 991px)': 3,
    '(min-width: 0px)': 2,
  };

  protected constructor(
    private breakpointObserver: BreakpointObserver,
    private googleTagManagerService: GoogleTagManagerService,
    private goToProductDetails: Command<string, boolean>,
    private goToAdvertisingItem: Command<AdvertiseCard, boolean>,
    private goToDesignTipCard: Command<Article>,
    private shuffledCardsPresenter: ShuffledCardsPresenter,
    pageForItems: string,
    pageForAdvertising: PromotionalImagesLanding
  ) {
    this.shuffledCards$ = this.shuffledCardsPresenter.values$;

    this.shopItemsForPage = pageForItems;
    this.advertisingForPage = pageForAdvertising;

    this.trackProducts();
  }

  ngOnInit() {
    this.breakpointObserver
      .observe(Object.keys(this.mediaBreakpointsToNumberColumns))
      .pipe(untilDestroyed(this), distinctUntilChanged())
      .subscribe((state: BreakpointState) => {
        const matchMediaBreakpoint = Object.entries(this.mediaBreakpointsToNumberColumns).find(
          (entry) => state.breakpoints[entry[0]]
        );
        if (matchMediaBreakpoint) {
          this.executeShuffle(matchMediaBreakpoint[1]);
        }
      });
  }

  convertAdCard(card: AdvertiseCard): AdvertisingCardData {
    return {
      id: '',
      imageSmall: card.backgroundImage,
      imageLarge: card.backgroundImageLarge,
      urlLink: card.linkText,
    } as AdvertisingCardData;
  }

  onAdvertisingCardClick(advertisingItem: AdvertiseCard) {
    this.goToAdvertisingItem.execute(advertisingItem);
  }

  onShoppingListDetailsObject(shopItem: ShopItem) {
    this.goToProductDetails.execute(shopItem.id);
  }

  onDesignTipsClick(article: Article) {
    this.goToDesignTipCard.execute(article);
  }

  executeShuffle(columnCount: number) {
    this.shuffledCardsPresenter.execute({
      shoppingItemType: this.shopItemsForPage,
      promotionalImagesLanding: this.advertisingForPage,
      columns: columnCount,
    });
  }

  private trackProducts() {
    this.shuffledCardsPresenter.values$
      .pipe(
        untilDestroyed(this),
        filter((items) => items.length > 0),
        tap((items) => {
          this.googleTagManagerService.trackEvent({
            props: {
              ecommerce: {
                currencyCode: 'EUR',
                impressions: items
                  .filter((item) => item.type === 'shop')
                  .map((item, index) => ({
                    id: item.shopItem!.id,
                    name: item.shopItem!.name,
                    position: `slot${index + 1}`,
                    brand: item.shopItem!.manufacturer,
                    category: 'Furniture',
                    list: 'my_Shoppinglist',
                  })),
              },
            },
          });
        })
      )
      .subscribe();
  }
}
