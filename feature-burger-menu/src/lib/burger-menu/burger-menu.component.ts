import { Component, Inject } from '@angular/core';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Command, Query } from '@rhinov/shared/cqrx';
import {
  ADD_PROPOSAL_COMMAND_TOKEN,
  GO_TO_EVALUATING_COMMAND_TOKEN,
  GO_TO_MY_PROJECTS_COMMAND_TOKEN,
  GO_TO_SHARING_COMMAND_TOKEN,
  IS_PROJECT_OWNER_QUERY_TOKEN,
  NEED_HELP_COMMAND_TOKEN,
} from '@rhinov/delivering/application';
import { IS_PROJECT_RATED_QUERY_TOKEN } from '@rhinov/evaluating/application';

@Component({
  selector: 'rhinov-burger-menu',
  templateUrl: './burger-menu.component.html',
  styleUrls: ['./burger-menu.component.scss'],
})
export class BurgerMenuComponent {
  isProjectOwner$: Observable<boolean>;
  isRateMyProjectButtonVisible$ = combineLatest([
    this.isProjectRated.values$,
    this.isProjectOwner.values$,
  ]).pipe(map(([isProjectRated, isProjectOwner]) => !isProjectRated && isProjectOwner));

  constructor(
    @Inject(IS_PROJECT_OWNER_QUERY_TOKEN)
    private isProjectOwner: Query<boolean>,

    @Inject(GO_TO_MY_PROJECTS_COMMAND_TOKEN)
    private goToMyProjectsCommand: Command,

    @Inject(GO_TO_EVALUATING_COMMAND_TOKEN)
    private rateMyProjectCommand: Command,

    @Inject(NEED_HELP_COMMAND_TOKEN)
    private needHelpCommand: Command,

    @Inject(GO_TO_SHARING_COMMAND_TOKEN)
    private shareMyProjectCommand: Command,

    @Inject(ADD_PROPOSAL_COMMAND_TOKEN)
    private askForEvolutionsCommand: Command,

    @Inject(IS_PROJECT_RATED_QUERY_TOKEN)
    private isProjectRated: Query<boolean>
  ) {
    this.isProjectOwner$ = this.isProjectOwner.values$;
  }

  onMyRoomsHandler() {
    this.goToMyProjectsCommand.execute();
  }

  onRateMyProjectHandler() {
    this.rateMyProjectCommand.execute();
  }

  onNeedHelpHandler() {
    this.needHelpCommand.execute();
  }

  onShareMyProjectHandler() {
    this.shareMyProjectCommand.execute();
  }

  onAskForEvolutionsHandler() {
    this.askForEvolutionsCommand.execute();
  }
}
