import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { FlexLayoutModule } from '@angular/flex-layout';

import { BurgerMenuComponent } from './burger-menu.component';
import { SharedPresentationModule } from '@rhinov/shared/presentation';
import { action } from '@storybook/addon-actions';
import { Command } from '@rhinov/shared/cqrx';
import {
  IS_PROJECT_OWNER_QUERY_TOKEN,
  ADD_PROPOSAL_COMMAND_TOKEN,
  GO_TO_EVALUATING_COMMAND_TOKEN,
  GO_TO_MY_PROJECTS_COMMAND_TOKEN,
  GO_TO_SHARING_COMMAND_TOKEN,
  NEED_HELP_COMMAND_TOKEN,
} from '@rhinov/delivering/application';
import { of } from 'rxjs';
import { boolean } from '@storybook/addon-knobs';
import { CustomError } from 'ts-custom-error';

export default {
  title: 'Feature| 2 - Burger menu',
  parameters: {
    viewport: {
      viewports: {
        ...INITIAL_VIEWPORTS,
      },
    },
  },
};

const queryIsProjectOwner = {
  execute: () => {},
  values$: of([{ isProjectOwner: boolean('isProjectOwner', true) }]),
};

const goToMyProjectsCommand: Command<boolean, boolean> = {
  execute: () => {},
  pending$: of(false),
  complete$: of(true),
  error$: of(new CustomError()),
};

const goToEvaluatingCommand: Command<boolean, boolean> = {
  execute: () => {},
  pending$: of(false),
  complete$: of(true),
  error$: of(new CustomError()),
};

const needHelpCommand: Command<boolean, boolean> = {
  execute: () => {},
  pending$: of(false),
  complete$: of(true),
  error$: of(new CustomError()),
};

const shareCommand: Command<boolean, boolean> = {
  execute: () => {},
  pending$: of(false),
  complete$: of(true),
  error$: of(new CustomError()),
};

const addProposalCommand: Command<boolean, boolean> = {
  execute: () => {},
  pending$: of(false),
  complete$: of(true),
  error$: of(new CustomError()),
};

export const BurgerMenuDesktop = () => ({
  moduleMetadata: {
    imports: [SharedPresentationModule, FlexLayoutModule],
    providers: [
      { provide: IS_PROJECT_OWNER_QUERY_TOKEN, useValue: queryIsProjectOwner },
      { provide: GO_TO_MY_PROJECTS_COMMAND_TOKEN, useValue: goToMyProjectsCommand },
      {
        provide: GO_TO_EVALUATING_COMMAND_TOKEN,
        useValue: goToEvaluatingCommand,
      },
      {
        provide: NEED_HELP_COMMAND_TOKEN,
        useValue: needHelpCommand,
      },
      {
        provide: GO_TO_SHARING_COMMAND_TOKEN,
        useValue: shareCommand,
      },
      {
        provide: GO_TO_SHARING_COMMAND_TOKEN,
        useValue: shareCommand,
      },
      {
        provide: ADD_PROPOSAL_COMMAND_TOKEN,
        useValue: addProposalCommand,
      },
    ],
  },
  component: BurgerMenuComponent,
  props: {
    onMyRoomsHandler: action('onMyRoomsHandler'),

    onRateMyProjectHandler: action('onRateMyProjectHandler'),

    onNeedHelpHandler: action('onNeedHelpHandler'),

    onShareMyProjectHandler: action('onShareMyProjectHandler'),

    onAskForEvolutionsHandler: action('onAskForEvolutionsHandler'),
  },
});
