import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { BurgerMenuComponent } from './burger-menu/burger-menu.component';
import { SharedPresentationModule } from '@rhinov/shared/presentation';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    SharedPresentationModule,
    RouterModule.forChild([{ path: '', component: BurgerMenuComponent }]),
  ],
  declarations: [BurgerMenuComponent],
})
export class FeatureBurgerMenuDesktopModule {}
